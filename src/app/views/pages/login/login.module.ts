import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MainLoginComponent } from './main-login/main-login.component';
import { AdminLoginComponent } from './admin-login/admin-login.component';
import { UserLoginComponent } from './user-login/user-login.component';



@NgModule({
  declarations: [MainLoginComponent, AdminLoginComponent, UserLoginComponent],
  imports: [
    CommonModule
  ]
})
export class LoginModule { }
