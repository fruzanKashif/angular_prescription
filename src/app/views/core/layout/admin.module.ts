// Angular
import { NgModule, } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { RouterModule, Routes } from '@angular/router';
import { DashboardComponent } from '../../pages/dashboard/dashboard.component';
import { AppointmentComponent } from '../../pages/dashboard/appointment/appointment.component';
import { DoctorsComponent } from '../../pages/dashboard/doctors/doctors.component';
import { DoctorListComponent } from '../../pages/dashboard/doctor/doctor-list/doctor-list.component';
import { DoctorYearlyComponent } from '../../pages/dashboard/doctor/doctor-yearly/doctor-yearly.component';
import { PatientListComponent } from '../../pages/dashboard/patient/patient-list/patient-list.component';
import { PatientsComponent } from '../../pages/dashboard/patients/patients.component';
import { RevenueComponent } from '../../pages/dashboard/revenue/revenue.component';

const routes: Routes = [
	{
        path: '',
        component: DashboardComponent,
        children: [
            {
                path: 'dashboard',
                component: DashboardComponent
            },
        ]
    }]
@NgModule({
	declarations: [
        DashboardComponent,
        AppointmentComponent,
        DoctorsComponent,
        DoctorListComponent,
        DoctorYearlyComponent,
        PatientListComponent,
        PatientsComponent,
        RevenueComponent,
	],
	imports: [
		CommonModule,
		FormsModule,
		RouterModule.forChild(routes)
	]
})
export class AdminModule {
}