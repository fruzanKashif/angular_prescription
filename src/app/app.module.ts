import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AdminHeaderComponent } from './views/core/layout/admin/admin-header/admin-header.component';
import { AdminFooterComponent } from './views/core/layout/admin/admin-footer/admin-footer.component';
import { AdminNotificationComponent } from './views/core/layout/admin/admin-notification/admin-notification.component';
import { AdminMenuComponent } from './views/core/layout/admin/admin-menu/admin-menu.component';
import { AdminComponent } from './views/core/layout/admin/admin.component';
import { AddPatientComponent } from './views/pages/patient/add-patient/add-patient.component';
import { ListPatientComponent } from './views/pages/patient/list-patient/list-patient.component';
import { PagesModule } from './views/pages/pages.module';

@NgModule({
  declarations: [
    AppComponent,
    AdminHeaderComponent,
    AdminFooterComponent,
    AdminNotificationComponent,
    AdminMenuComponent,
    AdminComponent,
    AddPatientComponent,
    ListPatientComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    PagesModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
